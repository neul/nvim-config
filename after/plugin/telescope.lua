local set = vim.keymap.set

--require('telescope').setup({
--  defaults = {
--        vimgrep_arguments = {
--          "rg",
--          "--color=never",
--          "--no-heading",
--          "--with-filename",
--          "--line-number",
--          "--column",
--          "--smart-case",
--          "-g", "!node_modules/**",
--          "-g", "!build/**",
--          "-g", "!dist/**",
--          "-g", "!target/**",
--      }, 
--      path_display = {"../*"},
--      file_sorter = require("telescope.sorters").get_fuzzy_file,
--      file_previewer = require("telescope.previewers").vim_buffer_cat.new,
--      generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
--      grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
--      qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
--  },
--  pickers = { 
--    find_files = {
--      find_command = {
--          "rg", 
--          "--files", 
--          "--hidden", 
--          "--ignore", "-u", 
--          "--glob=!**/.git/*", 
--          "--glob=!**/node_modules/*", 
--          "--glob=!**/.next/*"
--          }
--      }
--  },
--})

require('telescope').setup({
  opts = {
      cwd = "/"
  },
  defaults = {
       vimgrep_arguments = {
                "rg",
                "--color=never",
                "--no-heading",
                "--with-filename",
                "--line-number",
                "--column",
                "--smart-case",
                "--hidden",
                "--glob",
                "!**/.git/*",
      },
      file_sorter = require("telescope.sorters").get_fuzzy_file,
      file_previewer = require("telescope.previewers").vim_buffer_cat.new,
      generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
      grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
      qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
  },
  pickers = { 
    find_files = {
      find_command = {
          "rg", 
          "--files",
          }
      }
  },
})


require('telescope').load_extension('fzf')

local builtin = require('telescope.builtin')
set('n', '<leader>ff', builtin.find_files, {})
set('n', '<C-p>', builtin.git_files, {})
set('n', '<leader>fg', builtin.live_grep, {})
set('n', '<leader>fb', builtin.buffers, {})
set('n', '<leader>fh', builtin.help_tags, {})
set('n', '<leader>ft', builtin.treesitter, {})
set('n', '<leader>fa', builtin.builtin, {})
set('n', '<leader>ps', function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") })
end)

