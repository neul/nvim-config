vim.cmd.packadd('packer.nvim')

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  use "nvim-lua/plenary.nvim"
  use('nvim-tree/nvim-web-devicons')
  use 'nvim-lua/popup.nvim'

  use({'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'})
  use('nvim-treesitter/playground')
  use {
          'nvim-telescope/telescope.nvim', --tag = '0.1.1',
                                     branch = '0.1.x'
      }
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }

  use('theprimeagen/harpoon')
  use('mbbill/undotree')
  use('tpope/vim-fugitive')

  -- Lsp and Mason 
  use {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "neovim/nvim-lspconfig",
      }

 -- Autocompletion
  use {
        {'hrsh7th/nvim-cmp'},
  	    {'hrsh7th/cmp-buffer'},
  	    {'hrsh7th/cmp-path'},
  	    {'saadparwaiz1/cmp_luasnip'},
  	    {'hrsh7th/cmp-nvim-lsp'},
  	    {'hrsh7th/cmp-nvim-lua'},
        {'hrsh7th/cmp-cmdline'},
      }

  -- Snippets
  use {
          {'L3MON4D3/LuaSnip'},
          {'rafamadriz/friendly-snippets'},
      }

  -- Debugger
  use('mfussenegger/nvim-dap')
  use('rcarriga/nvim-dap-ui')

  use {
      'nvim-tree/nvim-tree.lua',
      tag = 'nightly' -- optional, updated every week. (see issue #1193)
  }

  use {'nvim-lualine/lualine.nvim'}
  use ('jose-elias-alvarez/null-ls.nvim')
  use ("ray-x/lsp_signature.nvim")

  -- Colorschemes
  use ("EdenEast/nightfox.nvim")
  use ("rose-pine/neovim")
  use ("olimorris/onedarkpro.nvim")

  -- Search 
  use {
      'phaazon/hop.nvim',
      branch = 'v2', -- optional but strongly recommended
      config = function()
        -- you can configure Hop the way you like here; see :h hop-config
      end
        }
    use 'ggandor/lightspeed.nvim'
end)
