--Enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.html.setup {
  capabilities = capabilities,
  init_options = {
      configurationSection = { "html", "css", "javascript", "python" },
      embeddedLanguages = {
        css = true,
        javascript = true, 
        python = true,
      },
      provideFormatter = true
    }
}


